<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainCategory extends Model
{
    protected $fillable = ['title'];
    protected $table = 'main_category';

    public function categories()
    {
        return $this->hasMany(Category::class, 'main_category_id', 'id');
    }
}
