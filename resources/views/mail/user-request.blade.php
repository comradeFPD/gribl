@component('mail::message')
{{ mb_strtoupper($category->title)}}<br>
Имя клиента: {{ $userRequest->name }}<br>
Телефон: {{ $userRequest->phone }}<br>
Способ связи: {{ $userRequest->type }}<br>
@foreach(json_decode($userRequest->info) as $key => $value)
    @if(is_array($value))
        {{ \App\Helpers\Helper::translate($key).': '.implode(',', $value) }}<br>
        @else
{{ \App\Helpers\Helper::translate($key).' : '.$value }}<br>
@endif
@endforeach
@if($userRequest->image_path !=null)
Фото желаемоей {{$category->title}}:
<img src="{{ asset('/storage/'.$userRequest->image_path) }}" alt="" width="400" height="400">
@endif
    @endcomponent
