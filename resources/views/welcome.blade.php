<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
    <meta name="description" content="Грибл.ру — сервис поиска выгодных услуг. Создайте единую заявку услуги у нас на сайте и получите выгодные предложения от компаний с выгодой до 20%. Теперь не нужно никуда звонить, никого искать. Одна ваша заявка — компании сами предложат выгодные условия. Это быстро, выгодно и бесплатно! Начать?" />
    <link rel="shortcut icon" href="favicon.png?v=3" type="image/x-icon">
    <title>Грибл.ру — сервис поиска выгодных услуг</title>
    <!--CSS	============================================= -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="css/linearicons.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="{{ asset('js/app.js') }}"></script>
</head>
<body id='top'>
<header id="header">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-between d-flex">
            <div id="logo">
                <a href="#top" class='logotop'><img src="images/logo.png" alt="" title="" style='max-width:200px;'/></a>
            </div>
            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li><a href="#how">Как это работает?</a></li>
                    <li><a href="#cats">Категории услуг</a></li>
                    <li><a href="#we">Это выгодно</a></li>
                    <li><a href="#faq">FAQ</a></li>
                    <li class="menu-has-children"><a href="#">Для производителей</a>
                        <ul>
                            <li><a href="{{ route('login') }}">Вход</a></li>
                            <li><a href="{{ route('register') }}">Регистрация</a></li>
                        </ul>
                    </li>
                </ul>
            </nav><!-- #nav-menu-container -->
        </div>
    </div>
</header>
<!-- Start Header Area -->

<!-- start Banner Area -->
<section class="home-banner-area relative">
    <div class="container-fluid">
        <div class="row  d-flex align-items-center justify-content-center">
            <div class="header-left col-lg-5 col-md-6 ">
                <h1>Создайте заявку на услугу и узнайте цены сразу от нескольких компаний, закажите у кого выгоднее по цене, срокам и тд.</h1>
                <p class="pt-20 pb-20">
                    Мы собрали у себя на сайте множества компаний, среди которых Вы выбираете наиболее выгодные варианты по цене, условиям и тд.
                </p>
                <div class="single-cause">
                    <div class="bottom d-flex justify-content-between align-items-center flex-wrap">
                        <a href="#" class="primary-btn offwhite select-city">{{ session('city') != null ? \App\City::find(session('city'))->title.' (изменить)' : 'Выбрать город' }}</a>
                        <a href="#cats" id='but_cats' class="primary-btn">Создать заявку</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-8 header-right">
                <div class="owl-carousel owl-banner">
                    <img class="img-fluid w-100" src="images/bg_top.jpg" alt="">
                    <img class="img-fluid w-100" src="images/bg_top2.jpg" alt="">
                    <img class="img-fluid w-100" src="images/bg_top3.jpg" alt="">
                    <img class="img-fluid w-100" src="images/bg_top4.jpg" alt="">
                    <img class="img-fluid w-100" src="images/bg_top5.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Banner Area -->

<!-- Start Causes Area -->
<section class="causes-area section-gap" id='how'>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7 section-title">
                <h2>Как это работает?</h2>
                <p>
                    Мы максимально упростили нашу систему, с помощью которой Вы сможете создать заявку, и получить расчет от компаний.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single-cause">
                    <div class="top">
                        <div class="thumb">
                            <img class="c-img img-fluid" src="images/1step.jpg" alt="">
                        </div>
                        <h3 class='text-center'>1. Создаете единую заявку</h3>
                        <p class="text text-center">
                            по необходимой услуге, указываете критерии заказа. <br/>Потратите не более 2 минут<br/>
                            пример: Натяжной потолок:
                            Глянцевый, 30 м2, установить гардину
                        </p>
                    </div>
                    <div class="bottom d-flex justify-content-between align-items-center flex-wrap">
                        <a href="#cats" id='but_cats2' class="primary-btn offwhite btn-block text-center">Выбрать услугу</a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="single-cause">
                    <div class="top">
                        <div class="thumb">
                            <img class="c-img img-fluid" src="images/2step.jpg" alt="">
                        </div>
                        <h3 class='text-center'>2. Мы отправляем заявку</h3>
                        <p class="text text-center">
                            на рассматрение в проверенные компании Вашего города для предоставления вам наиболее выгодного предложения.
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="single-cause">
                    <div class="top">
                        <div class="thumb">
                            <img class="c-img img-fluid" src="images/3step.jpg" alt="">
                        </div>
                        <h3 class='text-center'>3. Вы получаете предложения</h3>
                        <p class="text text-center">
                            в течении 30 минут, сравниваете, выбираете наиболее подходящее и выгодное для вас и начинаете работу с этой компанием.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end Causes Area -->

<!-- Start Service Area -->
<section class="collection-area section-gap" id='cats'>
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-lg-3 col-md-12">
                <div class="text pad-bot30">
                    <h3>Выберите нужную Вам услугу</h3>
                    <p>
                        Нажмите и перейдите на страницу создания заявки
                    </p>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-12 col-12">
                <div class="collection-box">
                    <p><img src='images/icons/popular.png'></p>
                    <p>Популярные услуги</p>
                    <p class="color3"><a href='cats/potolki'>Натяжные потолки</a></p>
                    <p class="color3"><a href='cats/kuhni'>Кухни на заказ</a></p>
                    <p class="color3"><a href='cats/shkafy'>Шкафы на заказ</a></p>
                </div>
            </div>
            @foreach($mainCategories as $main)
                <div class="col-lg-3 col-md-4 col-sm-12 col-12">
                    <div class="collection-box">
                        <p><img src='images/icons/mebel.png'></p>
                        <p>{{ $main->title }}</p>
                        @foreach($main->categories as $category)
                            <p class="color3"><a href='{{ url('/casts/'.$category->slug) }}'>{{ $category->title }}</a></p>
                            @endforeach
                    </div>
                </div>
                @endforeach
        </div>
    </div>
</section>
<!-- End Service Area -->
<div class="box-prem" id='we'>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7 section-title">
                <h2>Почему это выгодно?</h2>
            </div>
        </div>
        <div class="row row-prem" style='margin-top:0;'>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="box-part">
                    <div class="title">
                        <h3>Без переплат</h3>
                    </div>
                    <div class="text pad-bot30">
                        <span>Вы не переплачиваете, так как заказываете напрямую у компаний, без посредников, таким образом экономите Ваш бюджет.</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="box-part-bottom">
                    <img src='images/box-prem.png'>
                </div>
            </div>
        </div>
        <div class="row row-prem">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="box-part">
                    <div class="title">
                        <h3>Экономия Вашего времени</h3>
                    </div>
                    <div class="text pad-bot30">
                        <span>В 1 клик Вы видите несколько предложений от компаний и выбираете наиболее выгодные и подходящие для Вас по условиям цен, срокам и тд.</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="box-part-bottom">
                    <img src='images/box-prem2.png'>
                </div>
            </div>
        </div>
        <div class="row row-prem">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="box-part">
                    <div class="title">
                        <h3>Наш сервис работает бесплатно</h3>
                    </div>
                    <div class="text pad-bot30">
                        <span>Оформляя заявку на расчет цен по Вашей услуге в компании с помощью нашего сайта абсолютно бесплатно.</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="box-part-bottom">
                    <img src='images/box-prem3.png'>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="ask container-fluid" id="faq">
    <div class="container">
        <div class="row">
            <div class="col-12  wow fadeInUp" data-wow-duration="2s">
                <h2>Ответы на вопросы</h2>
            </div>
            <div class="col-12 col-md-6 ask-blocks">
                <div class="item item1">
                    <h4>Как называется ваш сервис?</h4>
                    <div>
                        <p>Приятно познакомиться, - Грибл.ру</p>
                        <p>Запоминайте и рассказывайте про нас своим друзьям :)</p>
                    </div>
                </div>
                <div class="item item2">
                    <h4>Это платно?</h4>
                    <div>
                        <p>Это быстро, выгодно и БЕСПЛАТНО!</p>
                        <p>Попробуйте, затем напишите нам в WhatsApp и расскажите о впечатлениях. <a href="http://wa.me/79528547251" target="_blank" style="display:inline-block;padding:0px 5px;color:#59C951;font-weight:bold;border-bottom:1px dashed #59C951;">Написать</a></p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 ask-blocks ask-blocks2">
                <div class="item item4">
                    <h4>Чем полезен ваш сайт?</h4>
                    <div>
                        <p>Представьте, что Вы хотите заказать кухню.</p>
                        <p>Вы открываете браузер, начинаете поиск, открываете много сайтов, смотрите фото, звоните в компании, чтобы задать вопросы, услышать ответы и рассказать какая кухню Вам нужна. НО, часто не берут трубки, не перезванивают, навязывают свои идеи, обманывают, и у вас нет никакого доверия к исполнителям.</p>
                        <p>На нашем сайте собраны проверянные компании, которые хотят помочь реализовать Вашу идею.</p>
                        <p>Вы просто переходите в нужный раздел, <a href="cats/kuhni" target="_blank" style="display:inline-block;padding:0px 5px;color:#59C951;font-weight:bold;border-bottom:1px dashed #59C951;">Кухни на заказ</a>, выбираете параметры кухни, какие Вы хотите(также можно загрузить фото), оставляете свои контакты и Ваша заявка отправляется компаниям вашего города, они ее просматривают, оценивают и отправляют Вам свое предложение, а Вы уже сами выбираете и решаете с кем начать сотрудничество. Мы экономим Вам деньги, время и нервы!</p>
                    </div>
                </div>
                <div class="item item5">
                    <h4>Как решить с какой компанием работать?</h4>
                    <div>
                        <p>В каждом разделе есть список компаний, с которыми Вы можете работать, напротив есть кнопка "Подробнее о компании".</p>
                        <p>Таким образом Вы сможете перейти на любую компанию, изучить её, посмотреть работы, связаться, почитать отзывы и сделать выбор.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- start footer Area -->
<footer class="footer-area section-gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6>Грибл.ру - сервис поиска выгодных услуг</h6>
                    <p>
                        Наша миссия создать сервис который поможет Вам просто и быстро заказать услугу без переплат!
                    </p>
                    <p class="footer-text"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy; 2019 - <script>document.write(new Date().getFullYear());</script> | Все права защищены</p>
                </div>
            </div>
            <div class="col-lg-5  col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6>Есть вопрос или предложение?</h6>
                    <p>Напишите нам, обязательно ответим!</p>
                    <div class="" id="mc_embed_signup">
                        <a href='https://wa.me/79528547251' target='_blank' class='btn btn-sm btn-success'><i class="fa fa-whatsapp"></i> WhatsApp</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6 social-widget">
                <div class="single-footer-widget">
                    <h6>Подписывайтесь</h6>
                    <p>Публикуем полезности</p>
                    <div class="footer-social d-flex align-items-center">
                        <a href="https://vk.com/greeble" target='_blank'><i class="fa fa-vk"></i></a>
                        <a href="https://instagram.com/greeble.ru"  target='_blank'><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- MODAL START !-->
<div class="modal fade" id="city" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h2>Выберите город</h2>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form action="#" enctype="multipart/form-data" method="post" id="city-form">
                    @csrf
                    <div class="form-group">
                        <select name="city" id="" class="custom-select">
                            @foreach($cities as $city)
                                <option value="{{ $city->id }}">{{ $city->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-default btn-success btn-block"><span class="glyphicon glyphicon-off"></span> Выбрать</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-default btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>Отменить</button>
            </div>
        </div>
    </div>
</div>
<!-- MODAL END -->
<!-- End footer Area -->
<script>
    $.fn.andSelf = function() {
        return this.addBack.apply(this, arguments);
    }
</script>

<script src="/js/superfish.min.js"></script>
<script src="/js/jquery.magnific-popup.min.js"></script>
<script src="/js/owl.carousel.min.js"></script>
<script src="/js/main.js"></script>
<script src="/js/js.js"></script>
<script>
    $(document).ready(function () {
        $('.select-city').click(function (e) {
            e.preventDefault();
            $('#city').modal();
        })
        $('#city-form').submit(function (e) {
            e.preventDefault();
            $.post('{{ route('select-city') }}', $('#city-form').serialize(), function (data) {
                $('#city').modal('hide');
                $('.select-city').text(data.city_name + ' (изменить)');
            })
        });
        $(".nav-menu li a:not(.menu-has-children ul li a), .logotop, #but_cats, #but_cats2, #mobile-nav li a").on("click", function(e){
            var anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $(anchor.attr('href')).offset().top - $("#header").height() - 10
            }, 777);
            e.preventDefault();
            return false;
        });
        $('#mobile-nav li a').on("click", function(e){
            $('body').removeClass("mobile-nav-active");
            $('#mobile-body-overly').hide();
            $('.lnr').removeClass('lnr-cross').addClass("lnr-menu");
        });
    });
</script>
</body>

</html>
