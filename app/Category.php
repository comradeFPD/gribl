<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function companies()
    {
        return $this->belongsToMany(Company::class, 'company_categories_pivot', 'category_id', 'company_id');
    }
}
