$(document).ready(function () {
    let checked = 0;
    $('input[type="checkbox"]').click(function () {
        checked = 0;
        $('input[type="checkbox"]').each(function (item, val) {
            if($(this).prop('checked') == true){
                checked++
            }
        });
        if(checked > 3){
            alert('Достигнуто максимальное кол-во категорий');
            return false;
        }
        $('.choose_service').text('Выбрано ('+checked+')');
    })
});
