<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Грибл.ру — быстрая регистрация компаний" />
    <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">
    <title>Грибл.ру — быстрая регистрация компаний</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/fast-reg.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--CSS	============================================= -->
    <script src="{{ asset('/js/app.js') }}"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#"><img src='images/logo-business.png' width='200'></a>
    <p>*Зарегиструйтесь и начнете получать заказы на свои услуги</p>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <a class="btn btn-outline-primary my-2 my-sm-0" href='/'>На главную</a>
        </form>
    </div>
</nav>
 @php
 $categories = \App\MainCategory::all();
$categories = $categories->split(2);
$cities = \App\City::all();
 @endphp


<div class="container">
    <div class="row">
        <form id="regForm" action="{{ route('register') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}

            <!-- One "tab" for each step in the form: -->
            <div class="tab box">
                <h4>3 предложения о грибл.ру:</h4>
                <hr class="colorgraph">
                <div class="row">
                    <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="feature-description">
                            <!-- feature-left -->
                            <div class="feature-left">
                                <div class="feature-icon"><i class="fa fa-check"></i></div>
                                <div class="feature-content">
                                    <p>С помощью нашего сервиса клиенты создают заявки на разные услуги от мебели на заказ до ведущего на свадьбу!</p>
                                </div>
                            </div>
                            <!-- /.feature-left -->
                            <!-- feature-left -->
                            <div class="feature-left">
                                <div class="feature-icon"><i class="fa fa-check"></i></div>
                                <div class="feature-content">
                                    <p>Мы ищем надежных исполнителей, знающие своё дело для передачи заказов!</p>
                                </div>
                            </div>
                            <!-- /.feature-left -->
                            <!-- feature-left -->
                            <div class="feature-left">
                                <div class="feature-icon"><i class="fa fa-check"></i></div>
                                <div class="feature-content">
                                    <p>Регистрируйтесь за 3 шага и получайте новых клиентов. Это бесплатно, без комиссий и тарифов! Успейте занять первые места в Вашем городе!</p>
                                </div>
                            </div>
                            <!-- /.feature-left -->
                        </div>
                    </div>
                    <!-- /.feature-sections -->
                    <div class="offset-xl-1 col-xl-6 offset-lg-1 col-lg-10 offset-md-1 col-md-10 col-sm-12 col-12 mt30">

                            <!-- service-form -->
                            <div class="service-form">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb10 ">
                                        <h4 class='text-left'>Заполните поля и нажмите «Далее»</h4>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 ">
                                        <div class="form-group service-form-group">
                                            <label class="control-label sr-only" for="name"></label>
                                            <input id="name" name="name" type="text" placeholder="Ваше имя" class="form-control" required>
                                            <div class="form-icon"><i class="fa fa-user"></i></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12  ">
                                        <div class="form-group service-form-group">
                                            <label class="control-label sr-only" for="phone"></label>
                                            <input id="phone" name="phone" type="text" placeholder="Введите телефон" class="form-control" required>
                                            <div class="form-icon"><i class="fa fa-phone"></i></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        <div class="form-group service-form-group">
                                            <label class="control-label sr-only" for="email"></label>
                                            <input id="email" name="email" type="email" placeholder="Введите почту (придет письмо)" class="form-control" required>
                                            <div class="form-icon"><i class="fa fa-envelope"></i></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-right">
                                        <button type="submit" id="nextBtn" class="btn btn-lg btn-primary">Далее</button>
                                    </div>
                                </div>
                            </div>

                        <!-- /.service-form -->
                    </div>
                </div>
            </div>
            <div class="tab box">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <h4>Отметьте те категории в которых Вы хотите получать заказы (не более 3-х) и нажмите кнопку «Далее»</h4>
                    <hr class="colorgraph">

                        <fieldset>
                            @foreach($categories as $mainCategory)
                                <div class="funkyradio">
                                    <div class='row'>
                                        @foreach($mainCategory as $item)
                                            <div class='col-xs-12 col-sm-4 col-md-4'>
                                                <h4>{{ $item->title }}</h4>
                                                @foreach($item->categories as $cat)
                                                    <div class="funkyradio-success">
                                                        <input type="checkbox" name="categories[]" value="{{ $cat->id }}" id="{{ 'checkbox'.$cat->id }}" />
                                                        <label for="{{'checkbox'.$cat->id }}">{{ $cat->title }}</label>
                                                    </div>
                                                    @endforeach
                                            </div>

                                            @endforeach
                                    </div>
                                </div>
                                @endforeach
                        </fieldset>

                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 text-right">
                        <div class='choose_service'>Выбрано (2)</div>
                        <div style="overflow:auto;">
                            <div>
                                <button type="button" id="prevBtn" class="btn btn-lg btn-secondary" onclick="nextPrev(-1)">Назад</button>
                                <button type="button" id="nextBtn" class="btn btn-lg btn-primary" onclick="nextPrev(1)">Далее</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab box">
                <h4>Вы почти у цели!</h4>
                <hr class="colorgraph">
                <div class="row">
                    <div class="col-xl-5 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="feature-description">
                            <!-- feature-left -->
                            <div class="feature-left">
                                <div class="feature-icon"><i class="fa fa-angle-double-right" style='position: relative;top: -15px;'></i></div>
                                <div class="feature-content">
                                    <p>Выберите ваш город из списка и нажмите кнопку «Завершить»</p>
                                </div>
                                <img src='images/cat-reg.jpg' style='max-width:100%;'>
                            </div>
                            <!-- /.feature-left -->
                        </div>
                    </div>
                    <!-- /.feature-sections -->
                    <div class="offset-xl-1 col-xl-6 offset-lg-1 col-lg-10 offset-md-1 col-md-10 col-sm-12 col-12 mt30">

                            <!-- service-form -->
                            <div class="service-form">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb10 ">
                                        <h4 class='text-left'>Заполните поля и нажмите «Далее»</h4>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group service-form-group">
                                            <select name="city" id="city">
                                                @foreach($cities as $item)
                                                    <option value="{{ $item->id }}">{{ $item->title }}</option>
                                                    @endforeach
                                            </select>
                                            <div class="form-icon"><i class="fa fa-home"></i></div>
                                        </div>
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-right">
                                        <button type="submit" class="btn btn-lg btn-primary finish">Завершить</button>
                                    </div>
                                </div>
                            </div>

                        <!-- /.service-form -->
                    </div>
                </div>
            </div>
    </div>
    <div class='clearfix'></div>
    <!-- Circles which indicates the steps of the form: -->
    <div class='container'>
        <div class='row'>
            <div style="text-align:center;padding-top:50px;display:table;margin:0 auto;">
                <span class="step"></span>
                <span class="step"></span>
                <span class="step"></span>
            </div>
        </div>
    </div>
    </form>
</div>
</div>

<div class="box-prem" id='we'>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7 section-title">
                <h2 class='text-center'>Почему это выгодно?</h2>
            </div>
        </div>
        <div class="row row-prem">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="box-part">
                    <div class="title">
                        <h3>Клиент сам соберет заявку по своим потребностям</h3>
                    </div>
                    <div class="text pad-bot30">
                        <span>Клиент выбрав Вашу услугу, создает по ней заявку с желаемыми параметрами с помощью удобного интерактивного инструмента</span>
                        <span>, а вы делаете клиенту горячее предложение благодаря тому, что знаете все его потребности.</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="box-part-bottom">
                    <img src='images/box-prem-biz.png'>
                </div>
            </div>
        </div>
        <div class="row row-prem">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="box-part">
                    <div class="title">
                        <h3>Не нужно мониторить заявки и сайт</h3>
                    </div>
                    <div class="text pad-bot30">
                        <span>Когда клиент создаст заявку по Вашей услуге, вам придет оповещение сразу автоматически.</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="box-part-bottom">
                    <img src='images/box-prem-biz2.png'>
                </div>
            </div>
        </div>
        <div class="row row-prem">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="box-part">
                    <div class="title">
                        <h3>Это бесплатно</h3>
                    </div>
                    <div class="text pad-bot30">
                        <span>Использование сервиса бесплатно, регистрируйтесь, начинайте получать заявки!</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="box-part-bottom">
                    <img src='images/box-prem3.png'>
                </div>
            </div>
        </div>
        <div class="row row-prem">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="box-part">
                    <div class="title">
                        <h3>В подарок получаете сайт</h3>
                    </div>
                    <div class="text pad-bot30">
                        <span>С вашей информацией, чтобы клиент имел возможность перейти на него, ознакомиться с вами, посмотреть работы и тд.</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="box-part-bottom">
                    <img src='images/box-prem-biz4.png'>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- start footer Area -->
<footer class="footer-area section-gap">
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6>Грибл.ру - сервис поиска выгодных услуг</h6>
                    <p>
                        Наша миссия создать сервис который поможет Вам просто и быстро заказать услугу без переплат!
                    </p>
                    <p class="footer-text"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy; 2019 - <script>document.write(new Date().getFullYear());</script> | Все права защищены</p>
                </div>
            </div>
            <div class="col-lg-5  col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6>Есть вопрос или предложение?</h6>
                    <p>Напишите нам, обязательно ответим!</p>
                    <div class="" id="mc_embed_signup">
                        <a href='https://wa.me/79528547251' target='_blank' class='btn btn-sm btn-success'><i class="fa fa-whatsapp"></i> WhatsApp</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-6 social-widget">
                <div class="single-footer-widget">
                    <h6>Подписывайтесь</h6>
                    <p>Публикуем полезности</p>
                    <div class="footer-social d-flex align-items-center">
                        <a href="https://vk.com/greeble" target='_blank'><i class="fa fa-vk"></i></a>
                        <a href="https://instagram.com/greeble.ru"  target='_blank'><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="js/fast-reg.js"></script>

<script>
    var currentTab = 0; // Current tab is set to be the first tab (0)
    showTab(currentTab); // Display the current tab

    function showTab(n) {
        // This function will display the specified tab of the form ...
        var x = document.getElementsByClassName("tab");
        x[n].style.display = "block";
        // ... and fix the Previous/Next buttons:
        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }
        /*if (n == (x.length - 1)) {
          document.getElementById("nextBtn").innerHTML = "Submit";
        } else {
          document.getElementById("nextBtn").innerHTML = "Next";
        }
        // ... and run a function that displays the correct step indicator:*/
        fixStepIndicator(n)
    }

    function nextPrev(n) {
        // This function will figure out which tab to display
        var x = document.getElementsByClassName("tab");
        // Exit the function if any field in the current tab is invalid:
        if (n == 1 && !validateForm()) return false;
        // Hide the current tab:
        x[currentTab].style.display = "none";
        // Increase or decrease the current tab by 1:
        currentTab = currentTab + n;
        // if you have reached the end of the form... :
        if (currentTab >= x.length) {
            //...the form gets submitted:
            document.getElementById("regForm").submit();
            return false;
        }
        // Otherwise, display the correct tab:
        showTab(currentTab);
    }

    function validateForm() {
        // This function deals with validation of the form fields
        var x, y, i, valid = true;
        x = document.getElementsByClassName("tab");
        y = x[currentTab].getElementsByTagName("input");
        // A loop that checks every input field in the current tab:
        for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
                // add an "invalid" class to the field:
                y[i].className += " invalid";
                // and set the current valid status to false:
                valid = false;
            }
        }
        // If the valid status is true, mark the step as finished and valid:
        if (valid) {
            document.getElementsByClassName("step")[currentTab].className += " finish";
        }
        return valid; // return the valid status
    }

    function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        var i, x = document.getElementsByClassName("step");
        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        //... and adds the "active" class to the current step:
        x[n].className += " active";
    }
    $('#nextBtn').click(function (e) {
        e.preventDefault();
        nextPrev(1);
    })


</script>

</body>
</html>
