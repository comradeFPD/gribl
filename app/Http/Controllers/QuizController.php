<?php

namespace App\Http\Controllers;

use App\Category;
use App\Company;
use App\Helpers\Helper;
use App\Jobs\SendUserRequestToCompanies;
use App\UserRequest;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    public function getView($slug)
    {
        $category = Category::where('slug', $slug)->firstOrFail();
        $company = $category->companies;
        return response()->view('quiz.main', [
            'category' => $category,
            'companies' => $company
        ]);
    }

    public function postSendRequest(Request $request)
    {
        $userRequest = UserRequest::create([
            'name' => $request->final_name,
            'phone' => $request->final_tel,
            'type' => $request->final_connect
        ]);
        $data = $request->except(['_token', 'final_name', 'final_tel', 'final_connect', 'category_id', 'upload_pic']);
        if($request->hasFile('photo')){
            $url = Helper::saveFile($request->file('upload_pic'), '/uploads/user-request');
            $userRequest->image_path = $url;
        }
        $userRequest->info = $data;
        $userRequest->save();
        $this->dispatch(new SendUserRequestToCompanies($userRequest, $request->category_id, $request->city_id));
        return response()->json('Ваша заявка успешно оставлена.', 200);

    }
}
