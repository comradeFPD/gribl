@extends('layouts.app')
<link rel="stylesheet" href="/css/owl.carousel.css">
<link rel="stylesheet" href="/css/quiz2.css">
<link href="/css/main2.css" rel="stylesheet">
@section('content')
<!-- start Banner Area -->
<section class="home-banner-area relative">
    <div class="container-fluid">
        <div class="row  d-flex align-items-center justify-content-center">
            <div class="header-left col-lg-5 col-md-6 ">
                <h1 class='h1_quiz'>Мы нашли в Краснодаре 10 компаний по услуге: «Кухни на заказ». <br/><br/>Создайте единую заявку, укажите критерии и получите выгодные предложения в течении 30 мин.</h1>
                <p class="pt-20 pb-20">
                    Мы собрали у себя на сайте множества компаний, среди которых Вы выбираете наиболее выгодные варианты по цене, условиям и тд.
                </p>
                <div class="single-cause">
                    <div class="bottom d-flex justify-content-between align-items-center flex-wrap">
                        <a href="#" class="primary-btn offwhite select-city">Выбрать город</a>
                        <a href="#cats" id='but_cats' class="primary-btn">Создать заявку</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 col-md-6 col-sm-8 header-right">
                <div class="owl-carousel owl-banner">
                    <img class="img-fluid w-100" src="/images/kitchen3.jpg" alt="">
                    <img class="img-fluid w-100" src="/images/kitchen2.jpg" alt="">
                    <img class="img-fluid w-100" src="/images/kitchen.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Banner Area -->

<div class="block__test" id="create_order">
    <div class="block__test_sub block__test_sub_2">
        <h2>Выберите форму будущей {{ $category->title }}</h2>
    </div>
    <div class="block__test_sub block__test_sub_3">
        <div class="block__test_sub_3_ready">Готово: <span> 0</span><span>%</span></div>
        <div class="block__test_sub_3_scale"><span style="width: 0%;"></span></div>
    </div>


 <div class="block__test_sub block__test_sub_4" data-sub-category="Кухни на заказ">
     <form action="{{ route('quiz.send-request') }}" id="request-form" method="post">
         {!! $category->form !!}
         {{--    <div class="block__test_sub_step data-slider__block active__step" data-title="Выберите форму будущей кухни" data-percent="0" data-step="true">
                <div class="block__test_sub_4_slider block__test_sub_4_slider_prev">
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24">
                        <path d="M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z"></path>
                    </svg>
                </div>
                <div class="block__test_sub_4_slider block__test_sub_4_slider_next">
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24">
                        <path d="M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z"></path>
                    </svg>
                </div>

                <div class="block__test_sub_4_move">
                    <div class="block__test_sub_4_move_img">
                        <label>
                            <input type="radio" name="texture" data-name="Форма кухни" value="Прямая">
                            <img src="/img/kitchens/forma_pramaya-min.jpg" alt="">
                            <span class="overlay">
                                    <span class="mark_choice">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 48 48"><path d="M18 32.34L9.66 24l-2.83 2.83L18 38l24-24-2.83-2.83z"></path></svg>
                                    </span>
                                </span>
                            <div>Прямая</div>
                        </label>
                    </div>
                    <div class="block__test_sub_4_move_img">
                        <label>
                            <input type="radio" name="texture" data-name="Форма кухни" value="Угловая">
                            <img src="/img/kitchens/forma_ugol-min.jpg" alt="">
                            <span class="overlay">
                                    <span class="mark_choice">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 48 48"><path d="M18 32.34L9.66 24l-2.83 2.83L18 38l24-24-2.83-2.83z"></path></svg>
                                    </span>
                                </span>
                            <div>Угловая</div>
                        </label>
                    </div>
                    <div class="block__test_sub_4_move_img">
                        <label>
                            <input type="radio" name="texture" data-name="Форма кухни" value="П - образная">
                            <img src="/img/kitchens/forma_p-min.jpg" alt="">
                            <span class="overlay">
                                    <span class="mark_choice">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 48 48"><path d="M18 32.34L9.66 24l-2.83 2.83L18 38l24-24-2.83-2.83z"></path></svg>
                                    </span>
                                </span>
                            <div>П-образная</div>
                        </label>
                    </div>
                    <div class="block__test_sub_4_move_img">
                        <label>
                            <input type="radio" name="texture" data-name="Форма кухни" value="С островом">
                            <img src="/img/kitchens/forma_ostrov-min.jpg" alt="">
                            <span class="overlay">
                                    <span class="mark_choice">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 48 48"><path d="M18 32.34L9.66 24l-2.83 2.83L18 38l24-24-2.83-2.83z"></path></svg>
                                    </span>
                                </span>
                            <div>С островом</div>
                        </label>
                    </div>
                    <div class="block__test_sub_4_move_img">
                        <label>
                            <input type="radio" name="texture" data-name="Форма кухни" value="С барной стойкой">
                            <img src="/img/kitchens/forma_bar-min.jpg" alt="">
                            <span class="overlay">
                                    <span class="mark_choice">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 48 48"><path d="M18 32.34L9.66 24l-2.83 2.83L18 38l24-24-2.83-2.83z"></path></svg>
                                    </span>
                                </span>
                            <div>С барной стойкой</div>
                        </label>
                    </div>
                </div>
            </div>
            <div class="block__test_sub_step data-slider__block" data-title="Выберите материал фасадов" data-name="Материал фасадов" data-percent="15" data-step="true">
                <div class="block__test_sub_4_slider block__test_sub_4_slider_prev">
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24">
                        <path d="M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z"></path>
                    </svg>
                </div>
                <div class="block__test_sub_4_slider block__test_sub_4_slider_next">
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24">
                        <path d="M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z"></path>
                    </svg>
                </div>
                <div class="multichoice">Выберите материал фасадов</div>
                <div class="block__test_sub_4_move">
                    <div class="block__test_sub_4_move_img">
                        <label>
                            <input type="checkbox" name="material[]" data-name="Массив дерева" value="Массив дерева">
                            <img src="/img/kitchens/fasad_massiv-min.jpg" alt="">
                            <span class="overlay">
                                    <span class="mark_choice">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 48 48"><path d="M18 32.34L9.66 24l-2.83 2.83L18 38l24-24-2.83-2.83z"></path></svg>
                                    </span>
                                </span>
                            <div>
                                Массив
                            </div>
                        </label>
                    </div>
                    <div class="block__test_sub_4_move_img">
                        <label>
                            <input type="checkbox" name="material[]" data-name="Пластик" value="Пластик">
                            <img src="/img/kitchens/fasad_plastic-min.jpg" alt="">
                            <span class="overlay">
                                    <span class="mark_choice">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 48 48"><path d="M18 32.34L9.66 24l-2.83 2.83L18 38l24-24-2.83-2.83z"></path></svg>
                                    </span>
                                </span>
                            <div>
                                Пластик
                            </div>
                        </label>
                    </div>
                    <div class="block__test_sub_4_move_img">
                        <label>
                            <input type="checkbox" name="material[]" data-name="МДФ" value="МДФ">
                            <img src="/img/kitchens/fasad_mdf.jpg" alt="">
                            <span class="overlay">
                                    <span class="mark_choice">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 48 48"><path d="M18 32.34L9.66 24l-2.83 2.83L18 38l24-24-2.83-2.83z"></path></svg>
                                    </span>
                                </span>
                            <div>
                                МДФ
                            </div>
                        </label>
                    </div>
                    <div class="block__test_sub_4_move_img">
                        <label>
                            <input type="checkbox" name="material[]" data-name="ЛДСП" value="ЛДСП">
                            <img src="/img/kitchens/ldsp.jpg" alt="">
                            <span class="overlay">
                                    <span class="mark_choice">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 48 48"><path d="M18 32.34L9.66 24l-2.83 2.83L18 38l24-24-2.83-2.83z"></path></svg>
                                    </span>
                                </span>
                            <div>
                                ЛДСП
                            </div>
                        </label>
                    </div>
                    <div class="block__test_sub_4_move_img">
                        <label>
                            <input type="checkbox" name="material[]" data-name="Шпон" value="Шпон">
                            <img src="/img/kitchens/fasad_shpon-min.jpg" alt="">
                            <span class="overlay">
                                    <span class="mark_choice">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 48 48"><path d="M18 32.34L9.66 24l-2.83 2.83L18 38l24-24-2.83-2.83z"></path></svg>
                                    </span>
                                </span>
                            <div>
                                Шпон
                            </div>
                        </label>
                    </div>
                </div>
            </div>
            <!-- НОВЫЙ ШАГ НАЧАЛО -->
            <div class="block__test_sub_step data-num__block" data-title="Введите примерную площадь кухни, погон. м" data-percent="30" data-step="true">
                    <fieldset data-quantity data-name="square">
                        <legend>Площадь кухни, погон. м</legend>
                    </fieldset>

                <div class='text-center' style="margin-top: 30px;">
                    <a href="javascript:void(0)" class="short__hint" data-toggle="popover" data-trigger="focus" data-content="<a href='https://greeble.ru/cats/img/kuhni/c.jpg' class='text-primary' target='_blank'>Как измерить длину кухни?</a>">
                        <div class="d-flex justify-content-center align-items-center align-content-center w-100">
                            <i class="fa fa-question"></i>
                        </div>
                    </a>
                </div>

            </div>
            <!-- ШАГ КОНЕЦ -->
            <div class="block__test_sub_step data-slider__block" data-title="Что Вы хотите разместить на кухне?" data-name="Встроенная техника" data-percent="60" data-step="true">
                <div class="block__test_sub_4_slider block__test_sub_4_slider_prev">
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24">
                        <path d="M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z"></path>
                    </svg>
                </div>
                <div class="block__test_sub_4_slider block__test_sub_4_slider_next">
                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 24 24">
                        <path d="M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z"></path>
                    </svg>
                </div>

                <div class="multichoice">Что вы хотите разместить на кухне</div>
                <div class="block__test_sub_4_move">

                    <div class="block__test_sub_4_move_img">
                        <label>
                            <input type="checkbox" name="kitchen_additional[]" data-name="Холодильник" value="Холодильник">
                            <img src="/img/kitchens/holod-min.jpg" alt="">
                            <span class="overlay">
                                    <span class="mark_choice">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 48 48"><path d="M18 32.34L9.66 24l-2.83 2.83L18 38l24-24-2.83-2.83z"></path></svg>
                                    </span>
                                </span>
                            <div>Холодильник</div>
                        </label>
                    </div>

                    <div class="block__test_sub_4_move_img">
                        <label>
                            <input type="checkbox" name="kitchen_additional[]" data-name="Варочная панель" value="Варочная панель">
                            <img src="/img/kitchens/var_panel-min.jpg" alt="">
                            <span class="overlay">
                                    <span class="mark_choice">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 48 48"><path d="M18 32.34L9.66 24l-2.83 2.83L18 38l24-24-2.83-2.83z"></path></svg>
                                    </span>
                                </span>
                            <div>Варочная панель</div>
                        </label>
                    </div>
                    <div class="block__test_sub_4_move_img">
                        <label>
                            <input type="checkbox" name="kitchen_additional[]" data-name="Духовой шкаф" value="Духовой шкаф">
                            <img src="/img/kitchens/duh_shkaf-min.jpg" alt="">
                            <span class="overlay">
                                    <span class="mark_choice">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 48 48"><path d="M18 32.34L9.66 24l-2.83 2.83L18 38l24-24-2.83-2.83z"></path></svg>
                                    </span>
                                </span>
                            <div>Духовой шкаф</div>
                        </label>
                    </div>
                    <div class="block__test_sub_4_move_img">
                        <label>
                            <input type="checkbox" name="kitchen_additional[]" data-name="Вытяжка" value="Вытяжка">
                            <img src="/img/kitchens/truba-min.jpg" alt="">
                            <span class="overlay">
                                    <span class="mark_choice">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 48 48"><path d="M18 32.34L9.66 24l-2.83 2.83L18 38l24-24-2.83-2.83z"></path></svg>
                                    </span>
                                </span>
                            <div>Вытяжка</div>
                        </label>
                    </div>
                    <div class="block__test_sub_4_move_img">
                        <label>
                            <input type="checkbox" name="kitchen_additional[]" data-name="Посудомоечная машина" value="Посудомоечная машина">
                            <img src="/img/kitchens/mashine.jpg" alt="">
                            <span class="overlay">
                                    <span class="mark_choice">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 48 48"><path d="M18 32.34L9.66 24l-2.83 2.83L18 38l24-24-2.83-2.83z"></path></svg>
                                    </span>
                                </span>
                            <div>Посудомоечная машина</div>
                        </label>
                    </div>
                </div>
            </div>
            <div class="block__test_sub_step data-options__block" data-title="Когда планируете?" data-percent="75" data-step="true">
                <div class="block__test_sub_4_step_2_options">
                    <label>
                        <input type="radio" data-name="Когда планирует" name="when" value="срочно">
                        <div><span class="mark_choice"></span></div>
                        <span class="square__text">Срочно</span>
                    </label>
                    <label>
                        <input type="radio" data-name="Когда планирует" name="when" value="в течении недели">
                        <div><span class="mark_choice"></span></div>
                        <span class="square__text">В течении недели</span>
                    </label>
                    <label>
                        <input type="radio" data-name="Когда планирует" name="when" value="в течении месяца">
                        <div><span class="mark_choice"></span></div>
                        <span class="square__text">В течении месяца</span>
                    </label>
                    <label>
                        <input type="radio" data-name="Когда планирует" name="when" value="пока не знаю">
                        <div><span class="mark_choice"></span></div>
                        <span class="square__text">Я не знаю</span>
                    </label>
                </div>
            </div>
            <div class="block__test_sub_step data-options__block" data-title="Какой бюджет вы планируете на кухню?" data-percent="90" data-step="true">
                <div class="block__test_sub_4_step_2_options">
                    <label>
                        <input type="radio" data-name="Бюджет кухни" name="price" value="до 50 тыс. руб">
                        <div><span class="mark_choice"></span></div>
                        <span class="square__text">До 50 тыс. руб</span>
                    </label>
                    <label>
                        <input type="radio" data-name="Бюджет кухни" name="price" value="От 50 до 100 тыс. руб">
                        <div><span class="mark_choice"></span></div>
                        <span class="square__text">От 50 до 100 тыс. руб</span>
                    </label>
                    <label>
                        <input type="radio" data-name="Бюджет кухни" name="price" value="От 100 до 150 тыс. руб">
                        <div><span class="mark_choice"></span></div>
                        <span class="square__text">От 100 до 150 тыс. руб</span>
                    </label>
                    <label>
                        <input type="radio" data-name="Бюджет кухни" name="price" value="От 150 до 200 тыс. руб">
                        <div><span class="mark_choice"></span></div>
                        <span class="square__text">От 150 до 200 тыс. руб</span>
                    </label>
                    <label>
                        <input type="radio" data-name="Бюджет кухни" name="price" value="свыше 200 т.р.">
                        <div><span class="mark_choice"></span></div>
                        <span class="square__text">Свыше 200 т.р.</span>
                    </label>
                </div>
            </div>
            <div class="block__test_sub_step data-pic__block" data-title="Если необходимо, прикрепите фото кухни, которая Вам понравилась" data-percent="95" data-step="false">
                <div class="form-group mx-auto add_file">
                    <label class="d-flex justify-content-center align-content-center align-items-center flex-column custom-file-label" for="upload_pic">
                        Если необходимо прикрепите фото кухни, которая Вам понравилась
                        <button class="btn btn-danger d-none" id="remove_pic">
                            <i class="fa fa-times" title="Удалить"></i>
                        </button>
                        <input type="file" class="custom-file-input" id="upload_pic" name="upload_pic" style="cursor:pointer;">
                    </label>
                </div>
            </div>
            <div class="block__test_sub_step" id="send_form_final" data-title="Оставьте свои контакты" data-percent="99" data-step="true">
                <div class="block__test_sub_4_step_3_form">
                        <div class="input__block">
                            <input type="text" name="final_name" data-name="Имя клиента" placeholder="Имя" pattern="[а-яА-Я\s\-_\w]{2,15}" title="Обязательное поле: 2-15 символов" required="true" autofocus="true">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18" class="input__icon" width="24" height="24">
                                <path d="M9 1C4.58 1 1 4.58 1 9s3.58 8 8 8 8-3.58 8-8-3.58-8-8-8zm0 2.75c1.24 0 2.25 1.01 2.25 2.25S10.24 8.25 9 8.25 6.75 7.24 6.75 6 7.76 3.75 9 3.75zM9 14.5c-1.86 0-3.49-.92-4.49-2.33C4.62 10.72 7.53 10 9 10c1.47 0 4.38.72 4.49 2.17-1 1.41-2.63 2.33-4.49 2.33z"></path>
                            </svg>
                        </div>
                        <div class="input__block">
                            <input type="tel" name="final_tel" data-name="Телефон" placeholder="Телефон" pattern="\+7\s\(\d{3}\)\s\d{3}-\d{2}-\d{2}" title="Введите действующий номер телефона" required="true">
                            <svg xmlns="http://www.w3.org/2000/svg" class="input__icon" viewBox="0 0 24 24" width="24" height="24"><path d="M6.62 10.79c1.44 2.83 3.76 5.14 6.59 6.59l2.2-2.2c.27-.27.67-.36 1.02-.24 1.12.37 2.33.57 3.57.57.55 0 1 .45 1 1V20c0 .55-.45 1-1 1-9.39 0-17-7.61-17-17 0-.55.45-1 1-1h3.5c.55 0 1 .45 1 1 0 1.25.2 2.45.57 3.57.11.35.03.74-.25 1.02l-2.2 2.2z"></path></svg>
                        </div>
                        <div class="input__block">
                            <select name="final_connect" id="" data-name="Способ связи" title="Обязательное поле: выберите способ связи" required="true">
                                <option value="0">Оставьте свои контакты</option>
                                <option value="Звонок">Звонок</option>
                                <option value="смс">СМС</option>
                                <option value="WhatsApp">WhatsApp</option>
                                <option value="Viber">Viber</option>
                                <option value="Telegram">Telegram</option>
                            </select>
                            <svg xmlns="http://www.w3.org/2000/svg" class="input__icon" viewBox="0 0 24 24" width="24" height="24"><path d="M10.09 15.59L11.5 17l5-5-5-5-1.41 1.41L12.67 11H3v2h9.67l-2.58 2.59zM19 3H5c-1.11 0-2 .9-2 2v4h2V5h14v14H5v-4H3v4c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z"></path></svg>
                        </div>
                        <div class="input__block">
                            <button class="button" type="submit">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" fill="#fff" width="22" height="22">
                                    <g><path d="M468.907,214.604c-11.423,0-20.682,9.26-20.682,20.682v20.831c-0.031,54.338-21.221,105.412-59.666,143.812c-38.417,38.372-89.467,59.5-143.761,59.5c-0.04,0-0.08,0-0.12,0C132.506,459.365,41.3,368.056,41.364,255.883c0.031-54.337,21.221-105.411,59.667-143.813c38.417-38.372,89.468-59.5,143.761-59.5c0.04,0,0.08,0,0.12,0c28.672,0.016,56.49,5.942,82.68,17.611c10.436,4.65,22.659-0.041,27.309-10.474c4.648-10.433-0.04-22.659-10.474-27.309c-31.516-14.043-64.989-21.173-99.492-21.192c-0.052,0-0.092,0-0.144,0c-65.329,0-126.767,25.428-172.993,71.6C25.536,129.014,0.038,190.473,0,255.861c-0.037,65.386,25.389,126.874,71.599,173.136c46.21,46.262,107.668,71.76,173.055,71.798c0.051,0,0.092,0,0.144,0c65.329,0,126.767-25.427,172.993-71.6c46.262-46.209,71.76-107.668,71.798-173.066v-20.842C489.589,223.864,480.33,214.604,468.907,214.604z"></path></g><g><path d="M505.942,39.803c-8.077-8.076-21.172-8.076-29.249,0L244.794,271.701l-52.609-52.609c-8.076-8.077-21.172-8.077-29.248,0c-8.077,8.077-8.077,21.172,0,29.249l67.234,67.234c4.038,4.039,9.332,6.058,14.625,6.058c5.293,0,10.586-2.019,14.625-6.058L505.942,69.052C514.019,60.975,514.019,47.88,505.942,39.803z"></path></g>
                                </svg>
                                <span>Отправить заявку</span>
                            </button>
                        </div>

                </div>
            </div> --}}
         {{ csrf_field() }}
         <input type="hidden" name="category_id" value="{{ $category->id }}">
         <input type="hidden" name="city_id" value="{{ session('city') == null ? '' : session('city') }}">
 </form>
 </div>
 <div class="block__test_sub block__test_sub_5">
     <div class="block__test_sub_5_btn">
         <a href="javascript:void(0)" class="button button-prev not-active">
             <svg viewBox="0 0 31.49 31.49" width="22" height="22">
                 <path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z"></path>
             </svg>
             <span>Назад</span>
         </a>
     </div>
     <div class="block__test_sub_5_btn">
         <a class="button button-next not-active" href="javascript:void(0)">
             <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" fill="#fff" width="22" height="22">
                 <g><path d="M468.907,214.604c-11.423,0-20.682,9.26-20.682,20.682v20.831c-0.031,54.338-21.221,105.412-59.666,143.812c-38.417,38.372-89.467,59.5-143.761,59.5c-0.04,0-0.08,0-0.12,0C132.506,459.365,41.3,368.056,41.364,255.883c0.031-54.337,21.221-105.411,59.667-143.813c38.417-38.372,89.468-59.5,143.761-59.5c0.04,0,0.08,0,0.12,0c28.672,0.016,56.49,5.942,82.68,17.611c10.436,4.65,22.659-0.041,27.309-10.474c4.648-10.433-0.04-22.659-10.474-27.309c-31.516-14.043-64.989-21.173-99.492-21.192c-0.052,0-0.092,0-0.144,0c-65.329,0-126.767,25.428-172.993,71.6C25.536,129.014,0.038,190.473,0,255.861c-0.037,65.386,25.389,126.874,71.599,173.136c46.21,46.262,107.668,71.76,173.055,71.798c0.051,0,0.092,0,0.144,0c65.329,0,126.767-25.427,172.993-71.6c46.262-46.209,71.76-107.668,71.798-173.066v-20.842C489.589,223.864,480.33,214.604,468.907,214.604z"></path></g><g><path d="M505.942,39.803c-8.077-8.076-21.172-8.076-29.249,0L244.794,271.701l-52.609-52.609c-8.076-8.077-21.172-8.077-29.248,0c-8.077,8.077-8.077,21.172,0,29.249l67.234,67.234c4.038,4.039,9.332,6.058,14.625,6.058c5.293,0,10.586-2.019,14.625-6.058L505.942,69.052C514.019,60.975,514.019,47.88,505.942,39.803z"></path></g>
             </svg>
             <span>Далее</span>
         </a>
     </div>
 </div>
</div>



<!-- start footer Area -->
<footer class="comps section-gap">
    <div class="container">
        <h2 class='text-center' style='margin-bottom:25px;'>Список исполнителей</h2>
        <div class="row">
            @foreach($companies as $company)
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="single-comp">
                        <a href='{{ url('/company/'.$company->id) }}'><img src='{{ url('/storage'.$company->company_logo) }}'></a>
                        <p>
                            <a href='{{ url('/company/'.$company->id) }}' class='btn btn-block btn-primary'>Подробнее</a>
                        </p>
                    </div>
                </div>
                @endforeach
        </div>
    </div>
</footer>
@endsection
@push('js')
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/jquery.inputmask.js"></script>
    <script src="/js/js.js"></script>
<script>
    $.fn.andSelf = function() {
        return this.addBack.apply(this, arguments);
    }
</script>

<script src="/js/superfish.min.js"></script>
<script src="/js/jquery.magnific-popup.min.js"></script>
<script src="/js/owl.carousel.min.js"></script>
<script src="/js/main.js"></script>
<script>
    $(document).ready(function () {
        $('.select-city').click(function (e) {
            e.preventDefault();
            $('#city').modal();
        })
        $('#city-form').submit(function (e) {
            e.preventDefault();
            $.post('{{ route("select-city") }}', $('#city-form').serialize(), function (data) {
                $('#city').modal('hide');
                $('.select-city').text(data.city_name + ' (изменить)');
            })
        });
        $(".nav-menu li a:not(.menu-has-children ul li a), .logotop, #but_cats, #but_cats2, #mobile-nav li a").on("click", function(e){
            var anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $(anchor.attr('href')).offset().top - $("#header").height() - 10
            }, 777);
            e.preventDefault();
            return false;
        });
        $('#mobile-nav li a').on("click", function(e){
            $('body').removeClass("mobile-nav-active");
            $('#mobile-body-overly').hide();
            $('.lnr').removeClass('lnr-cross').addClass("lnr-menu");
        });
        $('#request-form').submit(function (e) {
            e.preventDefault();
            $.ajax({
                url: '{{ route('quiz.send-request') }}',
                type: 'post',
                contentType: false,
                data: new FormData(this),
                processData: false,
                success: function (data) {
                    alert(data);
                }
            })

        })
    });
</script>
@endpush
