<?php

namespace App\Http\Controllers;

use App\City;
use App\Company;
use App\MainCategory;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function getCompanyView($id)
    {
        $company = Company::find($id);
        return response()->view('company.view', [
            'company' => $company
        ]);
    }

    public function postStoreCity(Request $request)
    {
        $city = City::find($request->city);
        session(['city' => $request->city]);
        return response()->json([
            'message' => 'Город '.$city->title.' выбран успешно',
            'city_name' => $city->title
        ], 200);
    }

    public function getIndex()
    {
        $cities = City::all();
        $mainCategories = MainCategory::all();
        return response()->view('welcome', [
            'mainCategories' => $mainCategories,
            'cities' => $cities
        ]);
    }
}
