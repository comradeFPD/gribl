<?php

namespace App\Http\Controllers;

use App\Category;
use App\City;
use App\Company;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('company');
        $this->middleware('check-company-status');
    }

    public function index()
    {
        $company = Auth::user()->company;
        $cities = City::all();
        $categories = Category::all();
        return response()->view('company.profile', [
            'company' => $company,
            'cities' => $cities,
            'categories' => $categories
        ]);
    }

    public function landingPage($id)
    {
        $company = Company::find($id);
        return response()->view('company.landing', [
            'company' => $company
        ]);
    }

    public function postUpdateInfo(Request $request)
    {
        $company = Auth::user()->company;
        $company->cities()->sync($request->cities);
        $company->categories()->sync($request->categories);
        $company->user->phone = $request->phone;
        $company->user->save();
        $company->reviews = $request->reviews != null ? $request->reviews : '';
        $company->description = $request->description;
        $company->company_title = $request->company_title;
        if($request->hasFile('company_logo')){
            $url = Helper::saveFile($request->file('company_logo'), '/uploads/company-logos');
            $company->company_logo = $url;
        }
        $company->save();
        return redirect()->back();
    }
}
