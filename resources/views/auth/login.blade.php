@extends('layouts.login')

@section('content')
    <section class="login-block">
        <div class="container">
            <div class='row' id='logo'>
                <a href='/'><img src='images/logo.png'></a>
            </div>
        </div>
        <div class="container">
            <div class="row login-block-row">
                <div class="col-md-4 login-sec">
                    <h2 class="text-center">Вход для Производителей</h2>
                    <form class="login-form" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <label for="login">Ваша почта</label>
                            <input type="text" class="form-control" name="email" placeholder="Введите почту" id='login' required>

                        </div>
                        <div class="form-group">
                            <label for="password">Ваш пароль</label>
                            <input type="password" class="form-control" name="password" placeholder="Введите пароль" id='password' required>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-login btn-block">Войти</button>
                        </div>
                    </form>
                    <div class="form-check2">
                        <a href='{{ route('password.request') }}' class='text-left'><small>Забыли пароль?</small></a>
                        <a href='{{ route('register') }}' class='text-right'><small>Регистрация</small></a>
                    </div>
                    <div class="copy-text">Сделанно с <i class="fa fa-heart"></i> by <a href='/' target='_blank'>Greeble.ru</a></div>
                </div>
                <div class="col-md-8 banner-sec">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <img class="d-block img-fluid" src="images/main_photo.jpg" alt="First slide">
                                <div class="carousel-caption d-none d-md-block">
                                    <div class="banner-text">
                                        <h2>Ваш личный кабинет</h2>
                                        <p>Позволит Вам видеть и обрабатывать заявки по Вашим услугам, Вы будете получать уже готовые от клиентов. Но также заявки будут приходить на почту.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>--}}
@endsection
