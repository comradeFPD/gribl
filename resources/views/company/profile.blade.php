@extends('layouts.app')
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
@section('content')
    <div class="container">
        <div class="col-md-12">
            <h1>Вы зашли под профилем компании</h1>
        </div>
        <div class="col-md-12">
            <form action="{{ route('company.update-info') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="name">Контактное лицо</label>
                    <input type="text" id="name" class="form-control" required name="name" value="{{ $company->name }}">
                </div>
                <div class="form-group">
                    <label for="company-title">Название компании</label>
                    <input type="text" required="company-title" class="form-control" name="company_title" value="{{ $company->company_title == null ? '' : $company->company_title }}">
                </div>
                <div class="form-group">
                    <label for="phone">Телефон</label>
                    <input type="tel" id="phone" class="form-control inputmask" name="phone" required placeholder="Введите ваш телефон" value="{{ $company->user->phone == null ? '' : $company->user->phone }}">
                </div>
                <div class="form-group">
                    <label for="reviews">Отзывы</label>
                    <textarea name="reviews" id="reviews" cols="30" rows="10">{!! $company->reviews != null ? $company->reviews : '' !!}</textarea>
                </div>
                <div class="form-group">
                    <label for="description">Описание компании</label>
                    <textarea name="description" id="description" cols="30" rows="10" required>{!! $company->description != null ? $company->description : '' !!}</textarea>
                </div>
                <div class="form-group">
                    <label for="photos">Логотип компании</label>
                    <input type="file" name="company_logo">
                    @if($company->company_logo != null)
                        <img src="{{ '/storage'.$company->company_logo }}" alt="">
                        @endif
                </div>
                <div class="form-group">
                    <label for="cities">Города где происходит обслуживание</label>
                    <select name="cities[]" class="custom-select" id="cities" multiple="multiple">
                        @foreach($cities as $city)
                            @if($company->cities->contains($city->id))
                                <option value="{{ $city->id }}" selected>{{ $city->title }}</option>
                                @else
                            <option value="{{ $city->id }}">{{ $city->title }}</option>
                            @endif
                            @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="categories">В каких категориях работает компания</label>
                    <select name="categories[]" class="custom-select" id="categories" multiple="multiple">
                        @foreach($categories as $category)
                            @if($company->categories->contains($category->id))
                                <option value="{{ $category->id }}" selected>{{ $category->title }}</option>
                                @else
                            <option value="{{ $category->id }}">{{ $category->title }}</option>
                            @endif
                            @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">Сохранить</button>
                </div>
            </form>
        </div>
    </div>
    @endsection
@push('js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script src="/js/ckeditor/ckeditor.js" ></script>
    <script src="/js/ckeditor/config.js" ></script>
    <script>
        $(document).ready(function () {
            $('.custom-select').select2();
            CKEDITOR.replace('reviews');
            CKEDITOR.replace('description');
        })
    </script>
    @endpush
