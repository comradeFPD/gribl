<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@getIndex');

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'company', 'middleware' => 'verified'], function (){
    Route::get('/', 'CompanyController@index')->name('company.index');
    Route::post('/update-info', 'CompanyController@postUpdateInfo')->name('company.update-info');
});

Route::get('/cats/{slug}', 'QuizController@getView');
Route::post('/send-request', 'QuizController@postSendRequest')->name('quiz.send-request');
Route::get('/company/{id}', 'MainController@getCompanyView');
Route::post('/select-city', 'MainController@postStoreCity')->name('select-city');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/email', function (){
    $markdown = new \Illuminate\Mail\Markdown(view(), config('mail.markdown'));
    return $markdown->render('mail.user-request', [
        'userRequest' => \App\UserRequest::find(3),
        'category' => \App\Category::find(1)
    ]);
});
