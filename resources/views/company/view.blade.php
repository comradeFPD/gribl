<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Название компании — описание" />
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon">
    <title>{{ $company->company_title }}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/comp.css">
    <!--CSS	============================================= -->
</head>
<body>
<div class='container_color'>
    <div class='menu_top'>
        <div class='container'>
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand" href="#"><img src='{{ asset('storage'.$company->company_logo) }}' style='width:160px; height: 100px'></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="/">Главная</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">О нас</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Оставить заявку</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Наши услуги</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Фото работ</a>
                        </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <p class="btn btn-success my-2 my-sm-0"><i class="fa fa-phone" aria-hidden="true"></i>{{ $company->user->phone }}</p>
                    </form>
                </div>
            </nav>
        </div>
    </div>
    <section class='card_main'>
        <div class='container'>
            <div class='row card_main_div'>
                <div class='card_main_div_img'>
                    <img src='/images/кухниолимп.png'>
                </div>
            </div>
        </div>
    </section>
</div>
<div class='info_comp'>
    <div class='container'>
        <div class='row info_comp_div'>
            <div class='col-md-7 info_comp_div_left'>
                <h2>{{ $company->company_title }}</h2>
                {!! $company->description !!}
            </div>
            <div class='col-md-5 info_comp_div_right'>
                <div class="form_order_comp">
                    <form action="" method="post" name="Login_Form" class="form-signin">
                        <h4 class="form-signin-heading">Оставьте заявку, мы с Вами свяжемся!</h4>
                        <hr class="colorgraph"><br>
                        <input type="text" class="form-control" name="telephone" placeholder="Ваш номер без 8" required="" />
                        <br/>
                        <button class="btn btn-lg btn-primary btn-block"  name="Submit" value="Login" type="Submit">Перезвоните</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class='photo_comp'>
    <div class='container'>
        <h1 class='text-center'>Фото работ</h1>
        <p class='text-center'>inappropriate behavior is often laughed off as</p>
    </div>
    <div class='container-fluid'>
        <div class='row photo_comp_row'>
            <div class='col-md-4 photo_comp_item'>
                <img src='/images/port.jpg'>
            </div>
            <div class='col-md-4 photo_comp_item'>
                <img src='/images/port2.jpg'>
            </div>
            <div class='col-md-4 photo_comp_item'>
                <img src='/images/port3.jpg'>
            </div>
        </div>
        <div class='row photo_comp_row'>
            <div class='col-md-4 photo_comp_item'>
                <img src='/images/port4.jpg'>
            </div>
            <div class='col-md-4 photo_comp_item'>
                <img src='/images/port3.jpg'>
            </div>
            <div class='col-md-4 photo_comp_item'>
                <img src='/images/port.jpg'>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
