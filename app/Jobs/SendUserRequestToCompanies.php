<?php

namespace App\Jobs;

use App\Company;
use App\Mail\UserRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendUserRequestToCompanies implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $userRequest;
    protected $categoryId;
    protected $city;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userRequest, $categoryId, $city)
    {
        $this->userRequest = $userRequest;
        $this->categoryId = $categoryId;
        $this->city = $city;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $catId = $this->categoryId;
        $city = $this->city;
        $companies = Company::where([
            'status' => 1
        ])->get();
        $companies = $companies->filter(function (Company $item) use ($catId){
            if($item->categories->count() > 0){
                foreach ($item->categories as $category) {
                    if($catId == $category->id)
                        return $item;
                }
            }
        });
        if($city != null){
            $companies = $companies->filter(function (Company $item) use ($city){
                if($item->cities->count() > 0){
                    foreach ($item->cities as $val){
                        if($city == $val->id ){
                            return $item;
                        }
                    }
                }
            });
        }
        foreach ($companies as $company){
            Mail::to($company->user->email)->send(new UserRequest($this->userRequest, $this->categoryId));
            if(env('MAIL_HOST', false) == 'smtp.mailtrap.io'){
                sleep(5);
            }
        }
    }
}
