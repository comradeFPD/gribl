<?php

use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $url = 'https://api.vk.com/method/database.getCities?country_id=1&need_all=0&access_token=b592eae1b592eae1b592eae125b5fc30edbb592b592eae1eb9be6d88632ce29fca0f51d&v=5.103';
        $headerOptions = array(
            'http' => array(
                'method' => "GET",
                'header' => "Accept-language: en\r\n" .
                    "Cookie: remixlang=0\r\n"
            )
        );
        $request = curl_init($url);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
        $resp = curl_exec($request);
        foreach (json_decode($resp)->response->items as $item){
            \App\City::create([
                'title' => $item->title
            ]);
        }
    }
}
