<?php

namespace App\Mail;

use App\Category;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRequest extends Mailable
{
    use Queueable, SerializesModels;
    protected $userRequest;
    protected $category;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userRequest, $category)
    {
        $this->userRequest = $userRequest;
        $this->category = Category::find($category);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Новая заявка!')->markdown('mail.user-request', [
            'userRequest' => $this->userRequest,
            'category' => $this->category
        ]);
    }
}
