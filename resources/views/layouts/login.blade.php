<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="description" content="Грибл.ру — сервис поиска выгодных услуг. Создайте единую заявку услуги у нас на сайте и получите выгодные предложения от компаний с выгодой до 20%. Теперь не нужно никуда звонить, никого искать. Одна ваша заявка — компании сами предложат выгодные условия. Это быстро, выгодно и бесплатно! Начать?" />

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <link rel="shortcut icon" href="favicon.png?v=3" type="image/x-icon">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="css/linearicons.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
</head>
<body>
<div id="app">

    <main class="py-4">
        @yield('content')
    </main>
</div>
@stack('js')
</body>
</html>
