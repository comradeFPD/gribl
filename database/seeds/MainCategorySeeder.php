<?php

use Illuminate\Database\Seeder;

class MainCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'Мебель на заказ',
            'Ремонт техники',
            'Безопастность',
            'Ремонт, строительство',
            'IT,  интернет, телеком',
            'Праздники, мероприятия',
            'Траспорт, перевозки, доставка',
            'Фото, видео',
        ];
        foreach ($categories as $category) {
            \App\MainCategory::create([
                'title' => $category
            ]);
        }
    }
}
