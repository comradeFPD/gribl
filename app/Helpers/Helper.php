<?php


namespace App\Helpers;


use Illuminate\Support\Facades\Storage;

class Helper
{
    public static function translate($word)
    {
        $translate = [
            'texture' => 'Тип кухни',
            'material' => 'Материал фасадов',
            'quantity' => 'Площадь кухни, погон. м.',
            'kitchen_additional' => 'Встроенная техника',
            'price' => 'Бюджет',
            'when' => 'Когда',
        ];
        return key_exists($word, $translate) ? $translate[$word] : '';
    }

    public static function saveFile($file, $path)
    {
        $localFile = $file;
        $extension = $localFile->getClientOriginalExtension();
        $filename = time().'.'.$extension;
        $localFile->move(storage_path('app/public'.$path), $filename);
        return $path.'/'.$filename;
    }
}
